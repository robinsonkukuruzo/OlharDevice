import ast
import requests
import subprocess
import configparser
import geocoder


class Services:
    """
    Класс, который предоставляет методы для выполнения bash- и python-скриптов,
    получения данных из JSON и выполнения скриптов на python.
    """

    def __init__(self):
        """
        Инициализирует объект Services.
        """
        pass

    def run_bash(self, script_path):
        """
        Запускает bash-скрипт.

        Аргументы:
        script_path (str): Путь к bash-скрипту.
        """
        try:
            subprocess.run(
                ["bash", script_path],
                capture_output=True,  # перехватывает вывод скрипта
            )
        except subprocess.CalledProcessError as e:
            print(f"Ошибка при выполнении bash скрипта: {e}")
            print(f"Ошибка при выполнении bash скрипта: {e}")

    def run_python(self, script_path):
        """
        Запускает python-скрипт.

        Аргументы:
        script_path (str): Путь к python-скрипту.
        """
        try:
            with open(script_path, 'r') as f:
                script = f.read()
            exec(script)
        except Exception as e:
            print(f"Ошибка при выполнении python скрипта: {e}")
            raise

    def fetch_json(self, url):
        """
        Запрашивает данные из JSON-запроса.

        Аргументы:
        url (str): URL для запроса.

        Возвращает:
        dict или None: Данные в формате словаря или None, если запрос не выполнен.
        """
        response = requests.post(url)
        
        if (response.status_code != 204 and response.status_code < 300 and response.headers["content-type"].strip().startswith("application/json")):
            try:
                return response.json()
            except ValueError:
                return response.status_code
        return None
    
    def fetch_script(self, url):
        """
        Запрашивает скрипт.

        Аргументы:
        url (str): URL для запроса.

        Возвращает:
        str или None: Текст скрипта или None, если запрос не выполнен.
        """

        response = requests.post(url)
        
        if response.status_code != 204:
            return response.text
        return None
    
    def is_valid_python(self, code):
        """
        Проверяет, является ли код валидным python-кодом.

        Аргументы:
        code (str): Код для проверки.

        Возвращает:
        bool: True, если код валиден, False в противном случае.
        """
        try:
            ast.parse(code)
        except SyntaxError:
            return False
        return True

    def is_valid_bash(self, code):
        """
        Проверяет, является ли код валидным bash-кодом.

        Аргументы:
        code (str): Код для проверки.

        Возвращает:
        bool: True, если код валиден, False в противном случае. 
        """

        result = subprocess.run(["pylint", "--errors-only", "-", code], capture_output=True, text=True)

        if result.returncode == 0:
            True
        else:
            False
    

    def register_video_view(video_id: int, equip_id: int, equip_ip: str, gps_lat: float, gps_lon: float):
        """
        Регистрация показа видео.

        Аргументы:
        video_id (int): Идентификатор видео.
        equip_id (int): Идентификатор оборудования.
        equip_ip (str): IP-адрес оборудования.
        gps_lat (float): Широта.
        gps_lon (float): Долгота.
        """
        url = "https://api.olhar.media/"
        params = {
            'regview': 1,
            'videoid': video_id,
            'equipid': equip_id,
            'equipip': equip_ip,
            'gpsposlat': gps_lat,
            'gpsposlon': gps_lon
        }
        
        response = requests.get(url, params=params)
        
        if response.status_code == 200:
            print("Регистрация показа видео прошла успешно.")
        else:
            print(f"Ошибка регистрации показа видео: {response.status_code}")
            
    def get_param_from_config(self, config_path: str, param_name: str):
        """
        Возвращает значение параметра из конфигурационного файла.

        Аргументы:
        config_path (str): Путь к конфигурационному файла.
        param_name (str): Имя параметра.

        Возвращает:
        str: Значение параметра или None, если параметр не найден.
        """

        config = configparser.ConfigParser()
        config.read(f'{config_path}')

        part_number = config['General'][f'{param_name}']

        return part_number

    def get_lat_lon(self):
        """
        Запрашивает координаты пользователя.

        Возвращает:
        tuple: Координаты пользователя в виде кортежа (широта, долгота).
        """

        # Запросить координаты пользователя с помощью библиотеки geocoder
        g = geocoder.ip('me')
        return g.latlng  
