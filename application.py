from PyQt5.QtWidgets import QMainWindow, QVBoxLayout, QWidget
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtCore import QUrl, pyqtSignal
from services import Service

service = Service()

# URL базы данных
BASE_URL = 'https://api.olhar.media/'

BASE_URL_VIDEO_ENDED = 'https://api.olhar.media/?regview=1'

class VideoPlayer(QWidget):
    """
    Виджет проигрывателя видео.
    """
    # Сигнал, указывающий на завершение проигрывания видео
    finished = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.setWindowTitle("Видеоплеер")
        self.setGeometry(100, 100, 800, 600)

        self.layout = QVBoxLayout(self)

        self.video_widget = QVideoWidget(self)
        self.layout.addWidget(self.video_widget)

        self.media_player = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        self.media_player.setVideoOutput(self.video_widget)
        # Подключение обработчика изменения статуса проигрывания
        self.media_player.mediaStatusChanged.connect(self.check_status)

    def show_video(self, video_url):
        """
        Отображение видео.

        Args:
            video_url (str): URL видео.
        """
        self.media_player.setMedia(QMediaContent(QUrl(video_url)))
        self.media_player.play()

    def check_status(self, status):
        """
        Проверка статуса проигрывания.

        Args:
            status (int): Статус проигрывания.
        """
        if status == QMediaPlayer.EndOfMedia:
            self.finished.emit()

class App(QMainWindow):
    """
    Главное окно приложения.
    """
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Видеоплеер")
        self.showFullScreen()

        self.central_widget = QWidget(self)
        self.setCentralWidget(self.central_widget)

        self.layout = QVBoxLayout(self.central_widget)

        self.video_player = VideoPlayer()
        self.layout.addWidget(self.video_player)

        self.current_video_index = 0
        self.video_list = []
        self.video_data = None

        # Подключение обработчика завершения проигрывания
        self.video_player.finished.connect(self.play_next_video)

    def load_video_data(self, video_data):
        """
        Загрузка списка видео.

        Args:
            video_data (list): Список видео.
        """
        self.video_data = video_data

    def load_videos(self, video_data):
        """
        Загрузка списка видео.

        Args:
            video_data (list): Список видео.
        """
        self.video_list = [BASE_URL + "/rk-videos/"+ video.serverfilename for video in video_data]
        if self.video_list:
            self.play_next_video(video_data)

    def play_next_video(self):
        """
        Запускает следующее видео из списка и регистрирует просмотр с помощью сервисного API.
        """

        # Если список видео не закончился
        if self.current_video_index < len(self.video_list):
            # Получаем URL текущего видео
            video_url = self.video_list[self.current_video_index]
            # Отображаем видео
            self.video_player.show_video(video_url)
            # Регистрируем просмотр
            # Необходимо передать идентификатор видео, идентификатор оборудования, IP-адрес оборудования,
            # широту и долготу.
            # Для получения параметров из конфигурационного файла используется сервисный класс
            service.register_video_view(
                video_id=self.video_data[self.current_video_index + 1]['id'],
                equip_id=int(service.get_param_from_config('config.ini', 'PN')),
                equip_ip="192.168.1.1",
                gpsposlat=service.get_lat_lon()[0],
                gpsposlon=service.get_lat_lon()[1]
            )
            # Увеличиваем индекс текущего видео
            self.current_video_index += 1


