import os
from services import Services 
from application import App
from PyQt5.QtWidgets import QApplication

base_url = 'https://api.olhar.media/'

service = Services()

try:
    script = service.fetch_script(f'{base_url}?getconfigupdate&equipid=1')
    if service.is_valid_bash(script):
        with open('script.sh', 'wb') as f:
            f.write(script.encode())
        service.run_bash('./script.sh')
        os.remove('./script.sh')
    elif service.is_valid_python(script):
        with open('script.py', 'wb') as f:
            f.write(script.encode())
        service.run_python('./script.py')
        os.remove('./script.py')
except Exception as e:
    pass

try:
    video_data = service.fetch_json(f'{base_url}?getvideos&equipid=1')
except Exception as e:
    raise e

if __name__ == "__main__":
    app = QApplication([])
    w = App()
    w.load_videos(video_data)
    w.show()
    app.exec_()

